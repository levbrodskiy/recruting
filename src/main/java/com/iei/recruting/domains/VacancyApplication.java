package com.iei.recruting.domains;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "vacancy_applications")
public class VacancyApplication {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "message")
    private String message;
    @ManyToOne
    @JoinColumn(name = "vacancy_id")
    private Vacancy vacancy;
    @ManyToOne
    @JoinColumn(name = "applicant_id")
    private Applicant applicant;
}

