package com.iei.recruting.domains;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Data
@Entity
@Table(name = "curriculum_vitae")
public class CurriculumVitae {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @OneToOne(optional = false)
    private Applicant owner;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @Column(name = "about_me")
    private String aboutMe;
    private String education;
    private String email;
    private String phone;
}
