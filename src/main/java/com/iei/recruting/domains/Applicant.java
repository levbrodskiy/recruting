package com.iei.recruting.domains;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "applicants")
public class Applicant {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "nickname")
    private String nickname;
    @OneToOne
    private User user;
    @OneToMany(mappedBy = "applicant", fetch = FetchType.LAZY)
    private List<VacancyApplication> vacancyApplications;
    @OneToOne(mappedBy = "owner")
    private CurriculumVitae curriculumVitae;
}
