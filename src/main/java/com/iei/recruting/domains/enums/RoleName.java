package com.iei.recruting.domains.enums;

public enum  RoleName {
    SIMPLE_APPLICANT, ADVANCED_APPLICANT, SIMPLE_COMPANY, ADVANCED_COMPANY
}
