package com.iei.recruting.domains;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "vacancies")
public class Vacancy {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(optional = false)
    private Company owner;
    @Column(name = "title")
    private String title;
    @Column(name = "description")
    private String description;
    @ManyToMany
    @JoinTable(name = "vacancies_categories",
    joinColumns = @JoinColumn(name = "vacancy_id"),
    inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories;
    @OneToMany(mappedBy = "vacancy", fetch = FetchType.EAGER)
    private List<VacancyApplication> vacancyApplications;

    public void addCategory(Category category) {
        categories.add(category);
    }

    public void removeCategory(long id) {
        categories.removeIf(category -> category != null &&
                category.getId() != null &&
                category.getId().equals(id));
    }

    public boolean categoryExists(long id) {
        return categories.
                stream().
                anyMatch(category -> category != null &&
                        category.getId() != null &&
                        category.getId().equals(id));
    }
}
