package com.iei.recruting.domains;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Data
@Entity
@Table(name = "companies")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
    @OneToOne
    private User user;
    @OneToMany(orphanRemoval = true, mappedBy = "owner")
    private List<Vacancy> vacancies;

    public boolean vacancyExists(long id) {
        return vacancies.stream().
                anyMatch(v -> v != null && v.getId() != null && v.getId().equals(id));
    }

    public Optional<Vacancy> getVacancy(long vacancyId) {
        return vacancies.
                stream().
                filter(
                        v -> v != null &&
                                v.getId() != null &&
                                v.getId().equals(vacancyId)).
                findFirst();
    }
}
