package com.iei.recruting.api.v1.controllers;

import com.iei.recruting.api.v1.requests.CurriculumVitaeRequest;
import com.iei.recruting.api.v1.responses.CurriculumVitaeResponse;
import com.iei.recruting.services.CurriculumVitaeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class CurriculumVitaeController {
    private final CurriculumVitaeService curriculumVitaeService;

    @Autowired
    public CurriculumVitaeController(CurriculumVitaeService curriculumVitaeService) {
        this.curriculumVitaeService = curriculumVitaeService;
    }

    @GetMapping("/curriculum_vitae")
    public ResponseEntity<?> getCvs() {
        List<CurriculumVitaeResponse> response =
                curriculumVitaeService.getCVs();

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/applicants/curriculum_vitae")
    public ResponseEntity<?> getCv() {
        CurriculumVitaeResponse response = curriculumVitaeService.getCvForCurrentApplicant();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/applicants/curriculum_vitae")
    public ResponseEntity<?> addCv(@Valid @RequestBody CurriculumVitaeRequest request) {
        CurriculumVitaeResponse response = curriculumVitaeService.addCvForCurrentApplicant(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/applicants/curriculum_vitae")
    public ResponseEntity<?> deleteCv() {
        curriculumVitaeService.deleteCvForCurrentApplicant();
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
