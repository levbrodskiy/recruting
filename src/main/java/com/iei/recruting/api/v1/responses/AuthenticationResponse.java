package com.iei.recruting.api.v1.responses;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class AuthenticationResponse {
    @JsonAlias(value = "access_token")
    private String accessToken;
}
