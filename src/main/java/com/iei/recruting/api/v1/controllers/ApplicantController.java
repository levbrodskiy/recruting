package com.iei.recruting.api.v1.controllers;

import com.iei.recruting.api.v1.responses.ApplicantResponse;
import com.iei.recruting.services.ApplicantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/applicants")
public class ApplicantController {
    private final ApplicantService applicantService;

    @Autowired
    public ApplicantController(ApplicantService applicantService) {
        this.applicantService = applicantService;
    }

    @GetMapping
    public ResponseEntity<?> getApplicants() {
        List<ApplicantResponse> response = applicantService.getApplicants();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
