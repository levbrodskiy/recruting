package com.iei.recruting.api.v1.responses;

import lombok.Data;

@Data
public class CompanyResponse {
    private Long id;
    private String name;
}
