package com.iei.recruting.api.v1.requests;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class ApplicantRegistrationRequest {
    @NotBlank(message = "must be not blank")
    @Size(min = 5, max = 256, message = "must be in range(5-256)")
    private String nickname;
    @NotBlank(message = "must be not blank")
    @Size(min = 5, max = 256, message = "must be in range(5-256)")
    @Email(message = "must be real")
    private String email;
    @NotBlank(message = "must be not blank")
    @Size(min = 5, max = 256, message = "must be in range(5-256)")
    private String password;
}
