package com.iei.recruting.api.v1.controllers;

import com.iei.recruting.api.v1.requests.ApplicantRegistrationRequest;
import com.iei.recruting.api.v1.requests.CompanyRegistrationRequest;
import com.iei.recruting.api.v1.responses.ApplicantRegistrationResponse;
import com.iei.recruting.api.v1.responses.CompanyRegistrationResponse;
import com.iei.recruting.services.ApplicantService;
import com.iei.recruting.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/registration")
public class RegistrationController {
    private final CompanyService companyService;
    private final ApplicantService applicantService;

    @Autowired
    public RegistrationController(CompanyService companyService, ApplicantService applicantService) {
        this.companyService = companyService;
        this.applicantService = applicantService;
    }

    @PostMapping("/company")
    public ResponseEntity<?> register(@Valid @RequestBody CompanyRegistrationRequest request) {
        CompanyRegistrationResponse response = companyService.register(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PostMapping("/applicant")
    public ResponseEntity<?> register(@Valid @RequestBody ApplicantRegistrationRequest request) {
        ApplicantRegistrationResponse response = applicantService.register(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }
}
