package com.iei.recruting.api.v1.controllers.auth;

import com.iei.recruting.api.v1.requests.AuthenticationRequest;
import com.iei.recruting.api.v1.responses.AuthenticationResponse;
import com.iei.recruting.services.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/v1")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseEntity<?> auth(@RequestBody AuthenticationRequest request) {
        try {
            AuthenticationResponse response =
                    authenticationService.authenticate(
                            request.getEmail(),
                            request.getPassword()
                    );
            return new ResponseEntity<>(response.getAccessToken(), HttpStatus.ACCEPTED);
        } catch (AuthenticationException e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PostMapping("/logout")
    public ResponseEntity<?> logout(HttpServletRequest request, HttpServletResponse response) {
        SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
        logoutHandler.logout(request, response, null);
        return null;
    }
}
