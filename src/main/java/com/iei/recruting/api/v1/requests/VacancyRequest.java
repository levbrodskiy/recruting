package com.iei.recruting.api.v1.requests;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class VacancyRequest {
    @NotBlank(message = "must be not blank")
    @Size(min = 5, max = 256, message = "must be in range(5-256)")
    private String title;
    @NotBlank(message = "must be not blank")
    @Size(min = 5, max = 256, message = "must be in range(5-256)")
    private String description;
}
