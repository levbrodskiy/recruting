package com.iei.recruting.api.v1.controllers;

import com.iei.recruting.api.v1.requests.VacancyApplicationRequest;
import com.iei.recruting.api.v1.responses.VacancyApplicationResponse;
import com.iei.recruting.services.VacancyApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class VacancyApplicationController {
    private final VacancyApplicationService vacancyApplicationService;

    @Autowired
    public VacancyApplicationController(VacancyApplicationService vacancyApplicationService) {
        this.vacancyApplicationService = vacancyApplicationService;
    }

    @GetMapping("/api/v1/companies/vacancies/{vacancy_id}/vacancy_applications")
    public ResponseEntity<?> getVacancyApplications(@PathVariable(name = "vacancy_id") Long id) {
        List<VacancyApplicationResponse> response =
                vacancyApplicationService.getVacancyApplicationsForVacancy(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/api/v1/applicants/vacancy_applications")
    public ResponseEntity<?> getVacancyApplications() {
        List<VacancyApplicationResponse> response =
                vacancyApplicationService.getVacancyApplicationsForCurrentApplicant();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/api/v1/applicants/vacancy_applications")
    public ResponseEntity<?> addVacancyApplication(@Valid @RequestBody VacancyApplicationRequest request) {
        VacancyApplicationResponse response =
                vacancyApplicationService.addVacancyApplicationForCurrentUser(request);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
