package com.iei.recruting.api.v1.responses;

import lombok.Data;

@Data
public class VacancyApplicationResponse {
    private Long id;
    private Long applicantId;
    private Long vacancyId;
    private String message;
}
