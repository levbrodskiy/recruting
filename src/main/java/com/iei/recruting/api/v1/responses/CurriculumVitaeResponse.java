package com.iei.recruting.api.v1.responses;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class CurriculumVitaeResponse {
    private Long id;
    @JsonAlias("applicant_id")
    private Long applicantId;
    private String title;
    private String description;
    @JsonAlias("about_me")
    private String aboutMe;
    private String education;
    private String email;
    private String phone;
}
