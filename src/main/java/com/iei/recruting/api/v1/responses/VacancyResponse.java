package com.iei.recruting.api.v1.responses;

import lombok.Data;

@Data
public class VacancyResponse {
    private Long id;
    private String title;
    private String description;
}

