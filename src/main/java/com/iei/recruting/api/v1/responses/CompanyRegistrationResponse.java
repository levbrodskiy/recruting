package com.iei.recruting.api.v1.responses;

import lombok.Data;

@Data
public class CompanyRegistrationResponse {
    private Long id;
    private String name;
}
