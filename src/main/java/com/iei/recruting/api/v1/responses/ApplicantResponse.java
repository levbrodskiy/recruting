package com.iei.recruting.api.v1.responses;

import lombok.Data;

@Data
public class ApplicantResponse {
    private Long id;
    private String nickname;
}

