package com.iei.recruting.api.v1.controllers;

import com.iei.recruting.api.v1.requests.VacancyRequest;
import com.iei.recruting.api.v1.responses.CategoryResponse;
import com.iei.recruting.api.v1.responses.VacancyResponse;
import com.iei.recruting.services.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/vacancies")
public class VacancyController {
    private final VacancyService vacancyService;

    @Autowired
    public VacancyController(VacancyService vacancyService) {
        this.vacancyService = vacancyService;
    }

    @GetMapping
    public ResponseEntity<?> getVacancies(
                @RequestParam(name = "category", required = false)String category
    ) {
        List<VacancyResponse> response = new ArrayList<>();
        if (category == null) {
            response  = vacancyService.getVacancies();
        } else {
            response = vacancyService.getVacancies(category);
        }

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> addVacancy(@Valid @RequestBody VacancyRequest request) {
        VacancyResponse response = vacancyService.addVacancyForCurrentCompany(request);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteVacancy(@PathVariable(name = "id") Long id) {
        vacancyService.deleteVacancyForCurrentCompany(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateVacancy(@PathVariable(name = "id") Long id,
                                           @Valid @RequestBody VacancyRequest request) {

        VacancyResponse response = vacancyService.updateVacancyForCurrentCompany(id, request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}/categories")
    public ResponseEntity<?> getVacancyCategories(@PathVariable(name = "id") Long id) {
        List<CategoryResponse> response = vacancyService.getVacancyCategories(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/{id}/categories")
    public ResponseEntity<?> addVacancyCategory(@PathVariable(name = "id") Long id,
                                                @Valid @RequestParam(name = "category_id") Long categoryId) {
        vacancyService.addCategoryToVacancyForCurrentCompany(id, categoryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}/categories")
    public ResponseEntity<?> deleteVacancyCategory(@PathVariable(name = "id") Long id,
                                                   @Valid @RequestParam(name = "category_id") Long categoryId) {
        vacancyService.deleteCategoryFromVacancyForCurrentCompany(id, categoryId);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
