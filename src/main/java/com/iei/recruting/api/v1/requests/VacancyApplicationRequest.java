package com.iei.recruting.api.v1.requests;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class VacancyApplicationRequest {
    @Positive(message = "must be positive")
    private Long vacancyId;
    @NotBlank(message = "must be not blank")
    @Size(min = 16, max = 256, message = "must be in range(16-256)")
    private String message;
}
