package com.iei.recruting.api.v1.controllers;

import com.iei.recruting.api.v1.responses.CompanyResponse;
import com.iei.recruting.services.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/companies")
public class CompanyController {
    private final CompanyService companyService;

    @Autowired
    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCompany(@PathVariable Long id) {
        CompanyResponse response = companyService.getCompany(id);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<?> getCompanies() {
        List<CompanyResponse> response = companyService.getCompanies();
        System.out.println(response);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
