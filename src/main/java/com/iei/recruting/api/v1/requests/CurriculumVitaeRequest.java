package com.iei.recruting.api.v1.requests;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class CurriculumVitaeRequest {
    @NotBlank(message = "must be not blank")
    @Size(min = 3, max = 256, message = "title must be in range(3-256)")
    private String title;
    @NotBlank(message = "must be not blank")
    @Size(min = 3, max = 256, message = "title must be in range(3-256)")
    private String description;
    @JsonAlias("about_me")
    private String aboutMe;
    private String education;
    private String email;
    private String phone;
}
