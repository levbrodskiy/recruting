package com.iei.recruting.api.v1.responses;

import lombok.Data;

@Data
public class ApplicantRegistrationResponse {
    private Long id;
    private String nickname;
}
