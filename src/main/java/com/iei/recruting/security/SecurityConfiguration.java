package com.iei.recruting.security;

import com.iei.recruting.domains.Role;
import com.iei.recruting.domains.enums.RoleName;
import com.iei.recruting.security.filters.JwtTokenFilter;
import com.iei.recruting.security.jwt.JwtAuthenticationEntryPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenFilter jwtTokenFilter;
    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Autowired
    public SecurityConfiguration(JwtTokenFilter jwtTokenFilter, JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint) {
        this.jwtTokenFilter = jwtTokenFilter;
        this.jwtAuthenticationEntryPoint = jwtAuthenticationEntryPoint;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.
                httpBasic().disable().
                csrf().disable().
                authorizeRequests().
                //antMatchers("/**").permitAll().
                antMatchers(HttpMethod.GET, "/api/v1/vacancies").
                hasAnyAuthority(RoleName.SIMPLE_COMPANY.name(), RoleName.ADVANCED_COMPANY.name(),
                        RoleName.SIMPLE_APPLICANT.name(), RoleName.ADVANCED_APPLICANT.name()).
                antMatchers(HttpMethod.POST, "/api/v1/vacancies").
                hasAnyAuthority(RoleName.SIMPLE_COMPANY.name(), RoleName.ADVANCED_COMPANY.name()).
                antMatchers(HttpMethod.PUT, "/api/v1/vacancies").
                hasAnyAuthority(RoleName.SIMPLE_COMPANY.name(), RoleName.ADVANCED_COMPANY.name()).
                antMatchers(HttpMethod.DELETE, "/api/v1/vacancies").
                hasAnyAuthority(RoleName.SIMPLE_COMPANY.name(), RoleName.ADVANCED_COMPANY.name()).
                antMatchers("/api/v1/login").permitAll().
                antMatchers("/api/v1/companies/").hasAnyAuthority(RoleName.SIMPLE_COMPANY.name(), RoleName.ADVANCED_COMPANY.name(),
                RoleName.SIMPLE_APPLICANT.name(), RoleName.ADVANCED_APPLICANT.name()).
                antMatchers("/api/v1/applicants/").hasAnyAuthority(RoleName.SIMPLE_COMPANY.name(), RoleName.ADVANCED_COMPANY.name(),
                RoleName.SIMPLE_APPLICANT.name(), RoleName.ADVANCED_APPLICANT.name()).
                antMatchers("/api/v1/registration/**").permitAll().
                anyRequest().
                authenticated().
                and().
                exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and().
                sessionManagement().
                sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(jwtTokenFilter, UsernamePasswordAuthenticationFilter.class);

    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return NoOpPasswordEncoder.getInstance();
    }
}
