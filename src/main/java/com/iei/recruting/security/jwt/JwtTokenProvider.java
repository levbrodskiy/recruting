package com.iei.recruting.security.jwt;

import com.iei.recruting.security.PersistentUserDetailsService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Component
public class JwtTokenProvider {
    @Value("${jwt.secret}")
    private String secret;
    @Value("${jwt.expiration}")
    private Long expirationTimeInMilliseconds;
    @Value("${jwt.authorizationHeader}")
    private String authorizationHeader;

    private final PersistentUserDetailsService userDetailsService;

    @Autowired
    public JwtTokenProvider(PersistentUserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

    public String createToken(String username) {
        Date expirationDate = new Date(new Date().getTime() + expirationTimeInMilliseconds);

        Claims claims = Jwts.
                claims().
                setSubject(username).
                setExpiration(expirationDate).
                setIssuedAt(new Date());
        return Jwts.
                builder().
                setClaims(claims).
                signWith(SignatureAlgorithm.HS256, secret).
                compact();
    }

    public boolean isValid(String token) {
        return Jwts.
                parser().
                setSigningKey(secret).
                parseClaimsJws(token).
                getBody().
                getExpiration().
                before(new Date());
    }

    public String getUserName(String token) {
        return Jwts.
                parser().
                setSigningKey(secret).
                parseClaimsJws(token).
                getBody().getSubject();
    }

    public Authentication getAuthentication(String token) throws JwtAuthenticationException {
        try {
            String username = getUserName(token);
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

            System.out.println(userDetails);

            return new UsernamePasswordAuthenticationToken(
                    userDetails.getUsername(),
                    userDetails.getPassword(),
                    userDetails.getAuthorities()
            );
        } catch (Exception e) {
            throw new JwtAuthenticationException("Auth exception", e);
        }
    }

    public Authentication getAuthentication(HttpServletRequest request) throws JwtAuthenticationException {
        String token = request.getHeader(authorizationHeader);
        return getAuthentication(token);
    }
}
