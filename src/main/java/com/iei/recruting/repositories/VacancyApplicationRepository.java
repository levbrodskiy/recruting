package com.iei.recruting.repositories;

import com.iei.recruting.domains.VacancyApplication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacancyApplicationRepository extends JpaRepository<VacancyApplication, Long> {
}
