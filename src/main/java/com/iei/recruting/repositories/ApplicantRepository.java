package com.iei.recruting.repositories;

import com.iei.recruting.domains.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicantRepository extends JpaRepository<Applicant, Long> {
    boolean existsByNickname(String nickname);
}
