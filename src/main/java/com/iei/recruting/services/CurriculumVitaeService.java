package com.iei.recruting.services;

import com.iei.recruting.api.v1.requests.CurriculumVitaeRequest;
import com.iei.recruting.api.v1.responses.CurriculumVitaeResponse;
import com.iei.recruting.domains.Applicant;
import com.iei.recruting.domains.Company;
import com.iei.recruting.domains.CurriculumVitae;
import com.iei.recruting.domains.User;
import com.iei.recruting.repositories.CurriculumVitaeRepository;
import com.iei.recruting.services.exceptions.EntityAlreadyExistsException;
import com.iei.recruting.services.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CurriculumVitaeService {
    private final CurriculumVitaeRepository curriculumVitaeRepository;
    private final SecurityService securityService;

    @Autowired
    public CurriculumVitaeService(CurriculumVitaeRepository curriculumVitaeRepository, SecurityService securityService) {
        this.curriculumVitaeRepository = curriculumVitaeRepository;
        this.securityService = securityService;
    }

    @Transactional
    public List<CurriculumVitaeResponse> getCVs() {
        List<CurriculumVitaeResponse> response =
                curriculumVitaeRepository.findAll().
                        stream().
                        map(curriculumVitae -> {
                            CurriculumVitaeResponse cv = new CurriculumVitaeResponse();
                            cv.setId(curriculumVitae.getId());
                            cv.setTitle(curriculumVitae.getTitle());
                            cv.setDescription(curriculumVitae.getDescription());
                            cv.setAboutMe(curriculumVitae.getAboutMe());
                            cv.setEducation(curriculumVitae.getEducation());
                            cv.setApplicantId(curriculumVitae.getOwner().getId());
                            cv.setEmail(curriculumVitae.getEmail());
                            cv.setPhone(curriculumVitae.getPhone());
                            return cv;
                        }).collect(Collectors.toList());

        return response;
    }

    @Transactional
    public void deleteCvForCurrentApplicant() {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Applicant applicant = user.getApplicant();

        if (applicant == null) {
            throw new EntityNotFoundException("Applicant not found");
        }

        if (applicant.getCurriculumVitae() == null) {
            throw new EntityNotFoundException("CV not found");
        }

        curriculumVitaeRepository.deleteById(applicant.getCurriculumVitae().getId());
    }

    @Transactional
    public CurriculumVitaeResponse addCvForCurrentApplicant(CurriculumVitaeRequest request) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Applicant applicant = user.getApplicant();

        if (applicant == null) {
            throw new EntityNotFoundException("Applicant not found");
        }

        if (applicant.getCurriculumVitae() != null) {
            throw new EntityAlreadyExistsException("CV already exists");
        }

        CurriculumVitae cv = new CurriculumVitae();
        cv.setId(null);
        cv.setTitle(request.getTitle());
        cv.setDescription(request.getDescription());
        cv.setAboutMe(request.getAboutMe());
        cv.setEducation(request.getEducation());
        cv.setEmail(request.getEmail());
        cv.setPhone(request.getPhone());
        cv.setOwner(applicant);

        applicant.setCurriculumVitae(cv);

        CurriculumVitae saved = curriculumVitaeRepository.save(cv);

        CurriculumVitaeResponse response = new CurriculumVitaeResponse();
        response.setId(saved.getId());
        response.setTitle(saved.getTitle());
        response.setDescription(saved.getDescription());
        response.setAboutMe(saved.getAboutMe());
        response.setEducation(saved.getEducation());
        response.setEmail(saved.getEmail());
        response.setPhone(saved.getPhone());
        response.setApplicantId(applicant.getId());

        return response;
    }

    @Transactional
    public CurriculumVitaeResponse getCvForCurrentApplicant() {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Applicant applicant = user.getApplicant();

        if (applicant == null) {
            throw new EntityNotFoundException("Applicant not found");
        }

        CurriculumVitae cv = applicant.getCurriculumVitae();

        if (cv == null) {
            throw new EntityNotFoundException("CV not found");
        }

        CurriculumVitaeResponse response = new CurriculumVitaeResponse();
        response.setId(cv.getId());
        response.setTitle(cv.getTitle());
        response.setDescription(cv.getDescription());
        response.setAboutMe(cv.getAboutMe());
        response.setEducation(cv.getEducation());
        response.setEmail(cv.getEmail());
        response.setPhone(cv.getPhone());
        response.setApplicantId(applicant.getId());

        return response;
    }
}
