package com.iei.recruting.services;

import com.iei.recruting.api.v1.requests.VacancyApplicationRequest;
import com.iei.recruting.api.v1.responses.VacancyApplicationResponse;
import com.iei.recruting.domains.*;
import com.iei.recruting.repositories.VacancyApplicationRepository;
import com.iei.recruting.repositories.VacancyRepository;
import com.iei.recruting.services.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Service
public class VacancyApplicationService {
    private final VacancyApplicationRepository vacancyApplicationRepository;
    private final VacancyRepository vacancyRepository;
    private final SecurityService securityService;

    @Autowired
    public VacancyApplicationService(VacancyApplicationRepository vacancyApplicationRepository, VacancyRepository vacancyRepository, SecurityService securityService) {
        this.vacancyApplicationRepository = vacancyApplicationRepository;
        this.vacancyRepository = vacancyRepository;
        this.securityService = securityService;
    }

    @Transactional
    public VacancyApplicationResponse addVacancyApplicationForCurrentUser(VacancyApplicationRequest request) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Applicant applicant = user.getApplicant();

        if (applicant == null) {
            throw new EntityNotFoundException("Applicant not found");
        }

        Vacancy vacancy = vacancyRepository.findById(request.getVacancyId()).
                orElseThrow(() -> new EntityNotFoundException("Vacancy not found"));

        VacancyApplication vacancyApplication = new VacancyApplication();
        vacancyApplication.setApplicant(applicant);
        vacancyApplication.setVacancy(vacancy);
        vacancyApplication.setMessage(request.getMessage());

        vacancyApplication = vacancyApplicationRepository.save(vacancyApplication);

        VacancyApplicationResponse response = new VacancyApplicationResponse();
        response.setId(vacancyApplication.getId());
        response.setMessage(vacancyApplication.getMessage());
        response.setApplicantId(applicant.getId());
        response.setVacancyId(vacancy.getId());

        return response;
    }

    @Transactional(readOnly = true)
    public List<VacancyApplicationResponse> getVacancyApplicationsForVacancy(Long vacancyId) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Company company = user.getCompany();

        if (company == null) {
            throw new EntityNotFoundException("Company not found");
        }

        Vacancy vacancy = vacancyRepository.findById(vacancyId).
                orElseThrow(() -> new EntityNotFoundException("Vacancy not found"));

        if (!vacancy.getOwner().getId().equals(company.getId())) {
            throw new EntityNotFoundException("Vacancy not found");
        }

        List<VacancyApplicationResponse> responseList = vacancy.getVacancyApplications().
                stream().map(vacancyApplication -> {
            VacancyApplicationResponse response = new VacancyApplicationResponse();
            response.setId(vacancyApplication.getId());
            response.setMessage(vacancyApplication.getMessage());
            response.setApplicantId(vacancyApplication.getApplicant().getId());
            response.setVacancyId(vacancy.getId());
            return response;
        }).collect(Collectors.toList());

        return responseList;
    }

    @Transactional(readOnly = true)
    public List<VacancyApplicationResponse> getVacancyApplicationsForCurrentApplicant() {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Applicant applicant = user.getApplicant();

        if (applicant == null) {
            throw new EntityNotFoundException("Applicant not found");
        }

        List<VacancyApplicationResponse> responseList = applicant.getVacancyApplications().
                stream().map(v -> {
            VacancyApplicationResponse response = new VacancyApplicationResponse();
            response.setId(v.getId());
            response.setMessage(v.getMessage());
            response.setVacancyId(v.getVacancy().getId());
            response.setApplicantId(v.getApplicant().getId());
            return response;
        }).collect(Collectors.toList());

        return responseList;
    }
}

