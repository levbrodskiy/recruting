package com.iei.recruting.services;

import com.iei.recruting.api.v1.responses.CategoryResponse;
import com.iei.recruting.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {
    private final CategoryRepository categoryRepository;

    @Autowired
    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Transactional
    public List<CategoryResponse> getCategories() {
        return categoryRepository.findAll().stream().map(c -> {
            CategoryResponse response = new CategoryResponse();
            response.setId(c.getId());
            response.setName(c.getName());
            response.setDescription(c.getDescription());
            return response;
        }).collect(Collectors.toList());
    }
}
