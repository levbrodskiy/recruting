package com.iei.recruting.services;

import com.iei.recruting.api.v1.requests.ApplicantRegistrationRequest;
import com.iei.recruting.api.v1.responses.ApplicantRegistrationResponse;
import com.iei.recruting.api.v1.responses.ApplicantResponse;
import com.iei.recruting.domains.Applicant;
import com.iei.recruting.domains.Role;
import com.iei.recruting.domains.User;
import com.iei.recruting.domains.enums.RoleName;
import com.iei.recruting.repositories.ApplicantRepository;
import com.iei.recruting.repositories.RoleRepository;
import com.iei.recruting.repositories.UserRepository;
import com.iei.recruting.services.exceptions.EntityAlreadyExistsException;
import com.iei.recruting.services.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ApplicantService {
    private final ApplicantRepository applicantRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public ApplicantService(ApplicantRepository applicantRepository, UserRepository userRepository, RoleRepository roleRepository) {
        this.applicantRepository = applicantRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Transactional
    public ApplicantRegistrationResponse register(ApplicantRegistrationRequest request) {
        if (userRepository.existsByEmail(request.getEmail())) {
            throw new EntityAlreadyExistsException("User with this email already exists");
        }

        if (applicantRepository.existsByNickname(request.getNickname())) {
            throw new EntityAlreadyExistsException("Applicant with this nickname already exists");
        }

        Role role  = roleRepository.findByName(RoleName.SIMPLE_APPLICANT).
                orElseThrow(() -> new EntityNotFoundException("Role not found"));

        User user = new User();
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        user.addRole(role);

        Applicant applicant = new Applicant();
        applicant.setNickname(request.getNickname());

        user.setApplicant(applicant);
        applicant.setUser(user);

        userRepository.save(user);
        applicantRepository.save(applicant);

        ApplicantRegistrationResponse response = new ApplicantRegistrationResponse();
        response.setId(user.getId());
        response.setNickname(applicant.getNickname());

        return response;
    }

    @Transactional
    public List<ApplicantResponse> getApplicants() {
        List<ApplicantResponse> responses = applicantRepository.findAll().stream().map(a -> {
            ApplicantResponse response = new ApplicantResponse();
            response.setId(a.getId());
            response.setNickname(a.getNickname());
            return response;
        }).collect(Collectors.toList());

        return responses;
    }

}
