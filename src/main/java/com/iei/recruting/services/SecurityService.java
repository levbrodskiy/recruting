package com.iei.recruting.services;

import com.iei.recruting.domains.User;
import com.iei.recruting.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class SecurityService {
    private final UserRepository userRepository;

    @Autowired
    public SecurityService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    public Optional<User> loadCurrentUser() {
        Authentication authentication =
                SecurityContextHolder.
                        getContext().
                        getAuthentication();

        if (authentication == null || authentication instanceof AnonymousAuthenticationToken) {
            return Optional.empty();
        }

        Optional<User> user = userRepository.findByEmail(authentication.getName());

        user.ifPresent(User::getRoles);

        return user;
    }
}
