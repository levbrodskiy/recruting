package com.iei.recruting.services;

import com.iei.recruting.api.v1.requests.VacancyRequest;
import com.iei.recruting.api.v1.responses.CategoryResponse;
import com.iei.recruting.api.v1.responses.VacancyResponse;
import com.iei.recruting.domains.Category;
import com.iei.recruting.domains.Company;
import com.iei.recruting.domains.User;
import com.iei.recruting.domains.Vacancy;
import com.iei.recruting.repositories.CategoryRepository;
import com.iei.recruting.repositories.VacancyRepository;
import com.iei.recruting.services.exceptions.EntityAlreadyExistsException;
import com.iei.recruting.services.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VacancyService {
    private final SecurityService securityService;
    private final VacancyRepository vacancyRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public VacancyService(SecurityService securityService, VacancyRepository vacancyRepository, CategoryRepository categoryRepository) {
        this.securityService = securityService;
        this.vacancyRepository = vacancyRepository;
        this.categoryRepository = categoryRepository;
    }

    @Transactional
    public List<VacancyResponse> getVacancies() {
        List<VacancyResponse> responses = vacancyRepository.findAll().stream().map(v -> {
            VacancyResponse response = new VacancyResponse();
            response.setId(v.getId());
            response.setTitle(v.getTitle());
            response.setDescription(v.getDescription());
            return response;
        }).collect(Collectors.toList());

        return responses;
    }

    @Transactional
    public List<VacancyResponse> getVacancies(String category) {
        List<VacancyResponse> responses = vacancyRepository.findAll().stream().filter(vacancy -> {
            return vacancy.getCategories().stream().anyMatch(c -> c.getName().equals(category));
        }).map(vacancy -> {

            VacancyResponse response = new VacancyResponse();
            response.setId(vacancy.getId());
            response.setTitle(vacancy.getTitle());
            response.setDescription(vacancy.getDescription());
            return response;
        }).collect(Collectors.toList());

        return responses;
    }

    @Transactional
    public VacancyResponse addVacancyForCurrentCompany(VacancyRequest request) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Company company = user.getCompany();

        if (company == null) {
            throw new EntityNotFoundException("Company not found");
        }

        Vacancy vacancy = new Vacancy();
        vacancy.setId(null);
        vacancy.setTitle(request.getTitle());
        vacancy.setDescription(request.getDescription());
        vacancy.setOwner(company);

        vacancy = vacancyRepository.save(vacancy);

        VacancyResponse response = new VacancyResponse();
        response.setId(vacancy.getId());
        response.setTitle(vacancy.getTitle());
        response.setDescription(vacancy.getDescription());

        return response;
    }

    @Transactional
    public VacancyResponse updateVacancyForCurrentCompany(Long id, VacancyRequest request) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Company company = user.getCompany();

        if (company == null) {
            throw new EntityNotFoundException("Company not found");
        }

        Vacancy vacancy = vacancyRepository.findById(id).
                orElseThrow(() -> new EntityNotFoundException("Vacancy with this id not found"));
        vacancy.setId(id);
        vacancy.setTitle(request.getTitle());
        vacancy.setDescription(request.getDescription());
        vacancy.setOwner(company);

        vacancy = vacancyRepository.save(vacancy);

        VacancyResponse response = new VacancyResponse();
        response.setId(vacancy.getId());
        response.setTitle(vacancy.getTitle());
        response.setDescription(vacancy.getDescription());

        return response;
    }

    @Transactional
    public void deleteVacancyForCurrentCompany(Long vacancyId) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Company company = user.getCompany();

        if (company == null) {
            throw new EntityNotFoundException("Company not found");
        }

        if (!company.vacancyExists(vacancyId)) {
            throw new EntityNotFoundException("Vacancy with this id not found");
        }

        vacancyRepository.deleteById(vacancyId);
    }

    @Transactional
    public void addCategoryToVacancyForCurrentCompany(Long vacancyId, Long categoryId) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Company company = user.getCompany();

        if (company == null) {
            throw new EntityNotFoundException("Company not found");
        }

        Vacancy vacancy = company.getVacancy(vacancyId).
                orElseThrow(() -> new EntityNotFoundException("Vacancy not found"));

        Category category = categoryRepository.findById(categoryId).
                orElseThrow(() -> new EntityNotFoundException("Category not found"));

        if (vacancy.categoryExists(categoryId)) {
            throw new EntityAlreadyExistsException("Category already exists");
        }

        vacancy.addCategory(category);

        vacancyRepository.save(vacancy);
    }

    @Transactional
    public void deleteCategoryFromVacancyForCurrentCompany(Long vacancyId, Long categoryId) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Company company = user.getCompany();

        if (company == null) {
            throw new EntityNotFoundException("Company not found");
        }

        Vacancy vacancy = company.getVacancy(vacancyId).
                orElseThrow(() -> new EntityNotFoundException("Vacancy not found"));

        if (!vacancy.categoryExists(categoryId)) {
            throw new EntityNotFoundException("Category not found");
        }

        vacancy.removeCategory(categoryId);

        vacancyRepository.save(vacancy);
    }

    @Transactional
    public List<CategoryResponse> getVacancyCategories(Long vacancyId) {
        User user = securityService.loadCurrentUser().
                orElseThrow(() -> new EntityNotFoundException("User not found"));

        Company company = user.getCompany();

        if (company == null) {
            throw new EntityNotFoundException("Company not found");
        }

        Vacancy vacancy = company.getVacancy(vacancyId).
                orElseThrow(() -> new EntityNotFoundException("Vacancy not found"));

        return vacancy.getCategories().stream().map(category -> {
            CategoryResponse response = new CategoryResponse();
            response.setId(category.getId());
            response.setName(category.getName());
            response.setDescription(category.getDescription());
            return response;
        }).collect(Collectors.toList());
    }
}
