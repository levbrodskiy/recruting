package com.iei.recruting.services;

import com.iei.recruting.api.v1.requests.CompanyRegistrationRequest;
import com.iei.recruting.api.v1.responses.CompanyRegistrationResponse;
import com.iei.recruting.api.v1.responses.CompanyResponse;
import com.iei.recruting.domains.Company;
import com.iei.recruting.domains.Role;
import com.iei.recruting.domains.User;
import com.iei.recruting.domains.enums.RoleName;
import com.iei.recruting.repositories.CompanyRepository;
import com.iei.recruting.repositories.RoleRepository;
import com.iei.recruting.repositories.UserRepository;
import com.iei.recruting.services.exceptions.EntityAlreadyExistsException;
import com.iei.recruting.services.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public CompanyService(CompanyRepository companyRepository, UserRepository userRepository, RoleRepository roleRepository) {
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @Transactional
    public CompanyRegistrationResponse register(CompanyRegistrationRequest request) {
        if (userRepository.existsByEmail(request.getEmail())) {
            throw new EntityAlreadyExistsException("User with this email already exists");
        }

        if (companyRepository.existsByName(request.getName())) {
            throw new EntityAlreadyExistsException("Company with this name already exists");
        }

        Role role  = roleRepository.findByName(RoleName.SIMPLE_COMPANY).
                orElseThrow(() -> new EntityNotFoundException("Role not found"));

        System.out.println(role);

        User user = new User();
        user.setEmail(request.getEmail());
        user.setPassword(request.getPassword());
        user.addRole(role);

        Company company = new Company();
        company.setName(request.getName());

        user.setCompany(company);
        company.setUser(user);

        user = userRepository.save(user);
        company = companyRepository.save(company);

        CompanyRegistrationResponse response = new CompanyRegistrationResponse();
        response.setId(user.getId());
        response.setName(request.getName());

        return response;
    }

    @Transactional
    public CompanyResponse getCompany(Long id) {
        Company company = companyRepository.findById(id).
                orElseThrow(() -> new EntityNotFoundException("Company with this id not found"));

        CompanyResponse response = new CompanyResponse();
        response.setId(company.getId());
        response.setName(company.getName());

        return response;
    }

    @Transactional
    public List<CompanyResponse> getCompanies() {
        List<CompanyResponse> responses = companyRepository.findAll().stream().flatMap(company -> {
            CompanyResponse response = new CompanyResponse();
            response.setId(company.getId());
            response.setName(company.getName());
            return Stream.of(response);
        }).collect(Collectors.toList());

        return responses;
    }
}
